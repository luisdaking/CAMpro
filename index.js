/**
 * Created by power on 21/06/16.
 */

var express = require('express');
var fs = require('fs');
var pt = require('path');
var app = express();
var exec = require('child_process').exec;
var child;

var size_max = 14E9; // 14 GByte

// executes `pwd`
var getlimit_SD = function (path) {
    var dirlist = fs.readdirSync(path);
    var size_dir = 0;
    var del_file = []
    for (i in dirlist) {
        size_dir += fs.statSync(path + dirlist[i]).size;
    }


    if (size_dir > size_max) {
        return {
            path: path,
            dirlist: dirlist,
            delfile: del_file,
            size: size_dir,
            state: true
        };
    }
    else {
        return {
            path: path,
            dirlist: dirlist,
            delfile: del_file,
            size: size_dir,
            state: false
        };
    }
};

var delFileSD = function (data_limit) {
    var d;
    d = fs.unlinkSync(data_limit.path + data_limit.dirlist[0])
    return d;
};


// carga plantilla
app.set('views', pt.join(__dirname, 'views'));
app.set('view engine', 'jade');

// leer configuracion
var cfg = JSON.parse(fs.readFileSync(pt.join(__dirname, "config.json")));

// funcion de captura de foto
// var CAMshot = function () {
//   console.log(new Date());
//   //child = exec("sudo gphoto2 --capture-image-and-download --filename \"IMG/canon-%Y%m%d-%H%M%S-%03n.%C\"", function (error, stdout, stderr) {
//   child = exec("sudo gphoto2 --capture-image-and-download --filename \"IMG/CAP000.%C\"", function (error, stdout, stderr) {
//     console.log('stdout: ' + stdout);
//     var infofile = getlimit_SD(__dirname + "/IMG/");
//     console.log(infofile)
//     var rename_file = fs.renameSync("IMG/CAP000.jpg")
//     if (error !== null) {
//       console.log('stderr: ' + stderr);
//       console.log('exec error: ' + error);
//     }
//   });
// };

function pad(n, width, z) {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

var CAMshot = function () {
  console.log(new Date());
  child = exec("sudo gphoto2 --capture-image-and-download --filename \"IMG/zzz.%C\"", function (error, stdout, stderr) {
    console.log('stdout: ' + stdout);
    var infofile = getlimit_SD("/root/IMG/");
    //console.log(infofile);
    if (error !== null) {
      console.log('stderr: ' + stderr);
      console.log('exec error: ' + error);
    }
  });


  var pathimg = "/root/IMG/";
  var infofile = getlimit_SD(pathimg);

  if(infofile.state){
    delFileSD(infofile);
  }

  //console.log(pathimg);
  //console.log(infofile);

  index_endFile = infofile.dirlist.indexOf("zzz.jpg");

  if(index_endFile >= 0){
    //console.log("\n\nExiste captura\n\n");
    //console.log("ind end: " + index_endFile);
    if(index_endFile == 0){
      fs.renameSync(pathimg + "zzz.jpg", pathimg + "000001.jpg");
    } else{
    // if(index_endFile == (infofile.dirlist.length - 1)){
      var numfile = parseInt(infofile.dirlist[index_endFile-1]) + 1;
      var nameNum = pad(numfile, 6)  + ".jpg";
      fs.renameSync(pathimg + "zzz.jpg", pathimg + nameNum);
    }
  }


};

// funcion de iteracion de captura de fotos
var CAMtask = setInterval(CAMshot, cfg.tiempo);

// servidor web y procesamiento de parametros GET HTTP
app.get('/', function (req, res) {
  //res.sendFile(pt.join(__dirname, 'index.html'));


  if(req.query.t !== undefined){
    clearInterval(CAMtask);
    if(req.query.t==0){
      clearInterval(CAMtask);
    }else{
      CAMtask = setInterval(CAMshot, req.query.t*1000);
    }

    cfg.tiempo = req.query.t*1000;
    fs.writeFileSync(pt.join(__dirname, "config.json"), JSON.stringify(cfg));
  }

  if(req.query.cl === "1"){
    CAMshot();
    console.log("Tomo foto");
  }

  res.render('index', {
    title: "Control remoto camara",
    tiempo: cfg.tiempo/1000
  });

});

// inicio de servidor web
app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});

